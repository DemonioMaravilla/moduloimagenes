/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class ListModuloImagenesData {
	private static final int ROW_COUNT = 100;
	
	public void listTestData() throws PersistentException {
		System.out.println("Listing User...");
		orm.User[] ormUsers = orm.UserDAO.listUserByQuery(null, null);
		int length = Math.min(ormUsers.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormUsers[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Imagen...");
		orm.Imagen[] ormImagens = orm.ImagenDAO.listImagenByQuery(null, null);
		length = Math.min(ormImagens.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormImagens[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Perfil...");
		orm.Perfil[] ormPerfils = orm.PerfilDAO.listPerfilByQuery(null, null);
		length = Math.min(ormPerfils.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormPerfils[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
	}
	
	public void listByCriteria() throws PersistentException {
		System.out.println("Listing User by Criteria...");
		orm.UserCriteria lormUserCriteria = new orm.UserCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormUserCriteria.id.eq();
		lormUserCriteria.setMaxResults(ROW_COUNT);
		orm.User[] ormUsers = lormUserCriteria.listUser();
		int length =ormUsers== null ? 0 : Math.min(ormUsers.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormUsers[i]);
		}
		System.out.println(length + " User record(s) retrieved."); 
		
		System.out.println("Listing Imagen by Criteria...");
		orm.ImagenCriteria lormImagenCriteria = new orm.ImagenCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormImagenCriteria.id.eq();
		lormImagenCriteria.setMaxResults(ROW_COUNT);
		orm.Imagen[] ormImagens = lormImagenCriteria.listImagen();
		length =ormImagens== null ? 0 : Math.min(ormImagens.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormImagens[i]);
		}
		System.out.println(length + " Imagen record(s) retrieved."); 
		
		System.out.println("Listing Perfil by Criteria...");
		orm.PerfilCriteria lormPerfilCriteria = new orm.PerfilCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormPerfilCriteria.id.eq();
		lormPerfilCriteria.setMaxResults(ROW_COUNT);
		orm.Perfil[] ormPerfils = lormPerfilCriteria.listPerfil();
		length =ormPerfils== null ? 0 : Math.min(ormPerfils.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormPerfils[i]);
		}
		System.out.println(length + " Perfil record(s) retrieved."); 
		
	}
	
	public static void main(String[] args) {
		try {
			ListModuloImagenesData listModuloImagenesData = new ListModuloImagenesData();
			try {
				listModuloImagenesData.listTestData();
				//listModuloImagenesData.listByCriteria();
			}
			finally {
				orm.ModuloImagenesPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
