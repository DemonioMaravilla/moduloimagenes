/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class DeleteModuloImagenesData {
	public void deleteTestData() throws PersistentException {
		PersistentTransaction t = orm.ModuloImagenesPersistentManager.instance().getSession().beginTransaction();
		try {
			orm.User lormUser = orm.UserDAO.loadUserByQuery(null, null);
			// Delete the persistent object
			orm.UserDAO.delete(lormUser);
			orm.Imagen lormImagen = orm.ImagenDAO.loadImagenByQuery(null, null);
			// Delete the persistent object
			orm.ImagenDAO.delete(lormImagen);
			orm.Perfil lormPerfil = orm.PerfilDAO.loadPerfilByQuery(null, null);
			// Delete the persistent object
			orm.PerfilDAO.delete(lormPerfil);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			DeleteModuloImagenesData deleteModuloImagenesData = new DeleteModuloImagenesData();
			try {
				deleteModuloImagenesData.deleteTestData();
			}
			finally {
				orm.ModuloImagenesPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
