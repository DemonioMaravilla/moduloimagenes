/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class CreateModuloImagenesData {
	public void createTestData() throws PersistentException {
		PersistentTransaction t = orm.ModuloImagenesPersistentManager.instance().getSession().beginTransaction();
		try {
			orm.User lormUser = orm.UserDAO.createUser();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : imagen, idPerfil, mail, pass, nick
			orm.UserDAO.save(lormUser);
			orm.Imagen lormImagen = orm.ImagenDAO.createImagen();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : ruta, idUser
			orm.ImagenDAO.save(lormImagen);
			orm.Perfil lormPerfil = orm.PerfilDAO.createPerfil();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : user, descripcion
			orm.PerfilDAO.save(lormPerfil);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			CreateModuloImagenesData createModuloImagenesData = new CreateModuloImagenesData();
			try {
				createModuloImagenesData.createTestData();
			}
			finally {
				orm.ModuloImagenesPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
