/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class RetrieveAndUpdateModuloImagenesData {
	public void retrieveAndUpdateTestData() throws PersistentException {
		PersistentTransaction t = orm.ModuloImagenesPersistentManager.instance().getSession().beginTransaction();
		try {
			orm.User lormUser = orm.UserDAO.loadUserByQuery(null, null);
			// Update the properties of the persistent object
			orm.UserDAO.save(lormUser);
			orm.Imagen lormImagen = orm.ImagenDAO.loadImagenByQuery(null, null);
			// Update the properties of the persistent object
			orm.ImagenDAO.save(lormImagen);
			orm.Perfil lormPerfil = orm.PerfilDAO.loadPerfilByQuery(null, null);
			// Update the properties of the persistent object
			orm.PerfilDAO.save(lormPerfil);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public void retrieveByCriteria() throws PersistentException {
		System.out.println("Retrieving User by UserCriteria");
		orm.UserCriteria lormUserCriteria = new orm.UserCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormUserCriteria.id.eq();
		System.out.println(lormUserCriteria.uniqueUser());
		
		System.out.println("Retrieving Imagen by ImagenCriteria");
		orm.ImagenCriteria lormImagenCriteria = new orm.ImagenCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormImagenCriteria.id.eq();
		System.out.println(lormImagenCriteria.uniqueImagen());
		
		System.out.println("Retrieving Perfil by PerfilCriteria");
		orm.PerfilCriteria lormPerfilCriteria = new orm.PerfilCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormPerfilCriteria.id.eq();
		System.out.println(lormPerfilCriteria.uniquePerfil());
		
	}
	
	
	public static void main(String[] args) {
		try {
			RetrieveAndUpdateModuloImagenesData retrieveAndUpdateModuloImagenesData = new RetrieveAndUpdateModuloImagenesData();
			try {
				retrieveAndUpdateModuloImagenesData.retrieveAndUpdateTestData();
				//retrieveAndUpdateModuloImagenesData.retrieveByCriteria();
			}
			finally {
				orm.ModuloImagenesPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
