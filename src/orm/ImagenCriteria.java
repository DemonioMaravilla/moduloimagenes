/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ImagenCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final IntegerExpression idUserId;
	public final AssociationExpression idUser;
	public final StringExpression ruta;
	public final StringExpression latitud;
	public final StringExpression longitud;
	public final StringExpression altura;
	public final DateExpression fecha;
	public final TimeExpression hora;
	public final IntegerExpression idCiudad;
	
	public ImagenCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		idUserId = new IntegerExpression("idUser.id", this);
		idUser = new AssociationExpression("idUser", this);
		ruta = new StringExpression("ruta", this);
		latitud = new StringExpression("latitud", this);
		longitud = new StringExpression("longitud", this);
		altura = new StringExpression("altura", this);
		fecha = new DateExpression("fecha", this);
		hora = new TimeExpression("hora", this);
		idCiudad = new IntegerExpression("idCiudad", this);
	}
	
	public ImagenCriteria(PersistentSession session) {
		this(session.createCriteria(Imagen.class));
	}
	
	public ImagenCriteria() throws PersistentException {
		this(orm.ModuloImagenesPersistentManager.instance().getSession());
	}
	
	public UserCriteria createIdUserCriteria() {
		return new UserCriteria(createCriteria("idUser"));
	}
	
	public Imagen uniqueImagen() {
		return (Imagen) super.uniqueResult();
	}
	
	public Imagen[] listImagen() {
		java.util.List list = super.list();
		return (Imagen[]) list.toArray(new Imagen[list.size()]);
	}
}

