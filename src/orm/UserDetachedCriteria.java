/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class UserDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression nick;
	public final StringExpression pass;
	public final StringExpression mail;
	public final IntegerExpression idPerfilId;
	public final AssociationExpression idPerfil;
	public final CollectionExpression imagen;
	
	public UserDetachedCriteria() {
		super(orm.User.class, orm.UserCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		nick = new StringExpression("nick", this.getDetachedCriteria());
		pass = new StringExpression("pass", this.getDetachedCriteria());
		mail = new StringExpression("mail", this.getDetachedCriteria());
		idPerfilId = new IntegerExpression("idPerfil.id", this.getDetachedCriteria());
		idPerfil = new AssociationExpression("idPerfil", this.getDetachedCriteria());
		imagen = new CollectionExpression("ORM_Imagen", this.getDetachedCriteria());
	}
	
	public UserDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.UserCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		nick = new StringExpression("nick", this.getDetachedCriteria());
		pass = new StringExpression("pass", this.getDetachedCriteria());
		mail = new StringExpression("mail", this.getDetachedCriteria());
		idPerfilId = new IntegerExpression("idPerfil.id", this.getDetachedCriteria());
		idPerfil = new AssociationExpression("idPerfil", this.getDetachedCriteria());
		imagen = new CollectionExpression("ORM_Imagen", this.getDetachedCriteria());
	}
	
	public PerfilDetachedCriteria createIdPerfilCriteria() {
		return new PerfilDetachedCriteria(createCriteria("idPerfil"));
	}
	
	public ImagenDetachedCriteria createImagenCriteria() {
		return new ImagenDetachedCriteria(createCriteria("ORM_Imagen"));
	}
	
	public User uniqueUser(PersistentSession session) {
		return (User) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public User[] listUser(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (User[]) list.toArray(new User[list.size()]);
	}
}

