/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ImagenDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final IntegerExpression idUserId;
	public final AssociationExpression idUser;
	public final StringExpression ruta;
	public final StringExpression latitud;
	public final StringExpression longitud;
	public final StringExpression altura;
	public final DateExpression fecha;
	public final TimeExpression hora;
	public final IntegerExpression idCiudad;
	
	public ImagenDetachedCriteria() {
		super(orm.Imagen.class, orm.ImagenCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		idUserId = new IntegerExpression("idUser.id", this.getDetachedCriteria());
		idUser = new AssociationExpression("idUser", this.getDetachedCriteria());
		ruta = new StringExpression("ruta", this.getDetachedCriteria());
		latitud = new StringExpression("latitud", this.getDetachedCriteria());
		longitud = new StringExpression("longitud", this.getDetachedCriteria());
		altura = new StringExpression("altura", this.getDetachedCriteria());
		fecha = new DateExpression("fecha", this.getDetachedCriteria());
		hora = new TimeExpression("hora", this.getDetachedCriteria());
		idCiudad = new IntegerExpression("idCiudad", this.getDetachedCriteria());
	}
	
	public ImagenDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.ImagenCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		idUserId = new IntegerExpression("idUser.id", this.getDetachedCriteria());
		idUser = new AssociationExpression("idUser", this.getDetachedCriteria());
		ruta = new StringExpression("ruta", this.getDetachedCriteria());
		latitud = new StringExpression("latitud", this.getDetachedCriteria());
		longitud = new StringExpression("longitud", this.getDetachedCriteria());
		altura = new StringExpression("altura", this.getDetachedCriteria());
		fecha = new DateExpression("fecha", this.getDetachedCriteria());
		hora = new TimeExpression("hora", this.getDetachedCriteria());
		idCiudad = new IntegerExpression("idCiudad", this.getDetachedCriteria());
	}
	
	public UserDetachedCriteria createIdUserCriteria() {
		return new UserDetachedCriteria(createCriteria("idUser"));
	}
	
	public Imagen uniqueImagen(PersistentSession session) {
		return (Imagen) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Imagen[] listImagen(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Imagen[]) list.toArray(new Imagen[list.size()]);
	}
}

