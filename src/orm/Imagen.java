/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Imagen {
	public Imagen() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_IMAGEN_IDUSER) {
			this.idUser = (orm.User) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.User idUser;
	
	private String ruta;
	
	private String latitud;
	
	private String longitud;
	
	private String altura;
	
	private java.util.Date fecha;
	
	private java.sql.Time hora;
	
	private Integer idCiudad;
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setRuta(String value) {
		this.ruta = value;
	}
	
	public String getRuta() {
		return ruta;
	}
	
	public void setLatitud(String value) {
		this.latitud = value;
	}
	
	public String getLatitud() {
		return latitud;
	}
	
	public void setLongitud(String value) {
		this.longitud = value;
	}
	
	public String getLongitud() {
		return longitud;
	}
	
	public void setAltura(String value) {
		this.altura = value;
	}
	
	public String getAltura() {
		return altura;
	}
	
	public void setFecha(java.util.Date value) {
		this.fecha = value;
	}
	
	public java.util.Date getFecha() {
		return fecha;
	}
	
	public void setHora(java.sql.Time value) {
		this.hora = value;
	}
	
	public java.sql.Time getHora() {
		return hora;
	}
	
	public void setIdCiudad(int value) {
		setIdCiudad(new Integer(value));
	}
	
	public void setIdCiudad(Integer value) {
		this.idCiudad = value;
	}
	
	public Integer getIdCiudad() {
		return idCiudad;
	}
	
	public void setIdUser(orm.User value) {
		if (idUser != null) {
			idUser.imagen.remove(this);
		}
		if (value != null) {
			value.imagen.add(this);
		}
	}
	
	public orm.User getIdUser() {
		return idUser;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_IdUser(orm.User value) {
		this.idUser = value;
	}
	
	private orm.User getORM_IdUser() {
		return idUser;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
