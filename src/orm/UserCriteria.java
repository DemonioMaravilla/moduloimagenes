/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class UserCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression nick;
	public final StringExpression pass;
	public final StringExpression mail;
	public final IntegerExpression idPerfilId;
	public final AssociationExpression idPerfil;
	public final CollectionExpression imagen;
	
	public UserCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		nick = new StringExpression("nick", this);
		pass = new StringExpression("pass", this);
		mail = new StringExpression("mail", this);
		idPerfilId = new IntegerExpression("idPerfil.id", this);
		idPerfil = new AssociationExpression("idPerfil", this);
		imagen = new CollectionExpression("ORM_Imagen", this);
	}
	
	public UserCriteria(PersistentSession session) {
		this(session.createCriteria(User.class));
	}
	
	public UserCriteria() throws PersistentException {
		this(orm.ModuloImagenesPersistentManager.instance().getSession());
	}
	
	public PerfilCriteria createIdPerfilCriteria() {
		return new PerfilCriteria(createCriteria("idPerfil"));
	}
	
	public ImagenCriteria createImagenCriteria() {
		return new ImagenCriteria(createCriteria("ORM_Imagen"));
	}
	
	public User uniqueUser() {
		return (User) super.uniqueResult();
	}
	
	public User[] listUser() {
		java.util.List list = super.list();
		return (User[]) list.toArray(new User[list.size()]);
	}
}

