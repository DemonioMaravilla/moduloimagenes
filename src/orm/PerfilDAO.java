/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class PerfilDAO {
	public static Perfil loadPerfilByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.ModuloImagenesPersistentManager.instance().getSession();
			return loadPerfilByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Perfil getPerfilByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.ModuloImagenesPersistentManager.instance().getSession();
			return getPerfilByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Perfil loadPerfilByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.ModuloImagenesPersistentManager.instance().getSession();
			return loadPerfilByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Perfil getPerfilByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.ModuloImagenesPersistentManager.instance().getSession();
			return getPerfilByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Perfil loadPerfilByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Perfil) session.load(orm.Perfil.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Perfil getPerfilByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Perfil) session.get(orm.Perfil.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Perfil loadPerfilByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Perfil) session.load(orm.Perfil.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Perfil getPerfilByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Perfil) session.get(orm.Perfil.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryPerfil(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.ModuloImagenesPersistentManager.instance().getSession();
			return queryPerfil(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryPerfil(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.ModuloImagenesPersistentManager.instance().getSession();
			return queryPerfil(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Perfil[] listPerfilByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.ModuloImagenesPersistentManager.instance().getSession();
			return listPerfilByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Perfil[] listPerfilByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.ModuloImagenesPersistentManager.instance().getSession();
			return listPerfilByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryPerfil(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Perfil as Perfil");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryPerfil(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Perfil as Perfil");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Perfil", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Perfil[] listPerfilByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryPerfil(session, condition, orderBy);
			return (Perfil[]) list.toArray(new Perfil[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Perfil[] listPerfilByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryPerfil(session, condition, orderBy, lockMode);
			return (Perfil[]) list.toArray(new Perfil[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Perfil loadPerfilByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.ModuloImagenesPersistentManager.instance().getSession();
			return loadPerfilByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Perfil loadPerfilByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.ModuloImagenesPersistentManager.instance().getSession();
			return loadPerfilByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Perfil loadPerfilByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Perfil[] perfils = listPerfilByQuery(session, condition, orderBy);
		if (perfils != null && perfils.length > 0)
			return perfils[0];
		else
			return null;
	}
	
	public static Perfil loadPerfilByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Perfil[] perfils = listPerfilByQuery(session, condition, orderBy, lockMode);
		if (perfils != null && perfils.length > 0)
			return perfils[0];
		else
			return null;
	}
	
	public static java.util.Iterator iteratePerfilByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.ModuloImagenesPersistentManager.instance().getSession();
			return iteratePerfilByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iteratePerfilByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.ModuloImagenesPersistentManager.instance().getSession();
			return iteratePerfilByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iteratePerfilByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Perfil as Perfil");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iteratePerfilByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Perfil as Perfil");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Perfil", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Perfil createPerfil() {
		return new orm.Perfil();
	}
	
	public static boolean save(orm.Perfil perfil) throws PersistentException {
		try {
			orm.ModuloImagenesPersistentManager.instance().saveObject(perfil);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Perfil perfil) throws PersistentException {
		try {
			orm.ModuloImagenesPersistentManager.instance().deleteObject(perfil);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Perfil perfil)throws PersistentException {
		try {
			orm.User[] lUsers = perfil.user.toArray();
			for(int i = 0; i < lUsers.length; i++) {
				lUsers[i].setIdPerfil(null);
			}
			return delete(perfil);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Perfil perfil, org.orm.PersistentSession session)throws PersistentException {
		try {
			orm.User[] lUsers = perfil.user.toArray();
			for(int i = 0; i < lUsers.length; i++) {
				lUsers[i].setIdPerfil(null);
			}
			try {
				session.delete(perfil);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Perfil perfil) throws PersistentException {
		try {
			orm.ModuloImagenesPersistentManager.instance().getSession().refresh(perfil);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Perfil perfil) throws PersistentException {
		try {
			orm.ModuloImagenesPersistentManager.instance().getSession().evict(perfil);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Perfil loadPerfilByCriteria(PerfilCriteria perfilCriteria) {
		Perfil[] perfils = listPerfilByCriteria(perfilCriteria);
		if(perfils == null || perfils.length == 0) {
			return null;
		}
		return perfils[0];
	}
	
	public static Perfil[] listPerfilByCriteria(PerfilCriteria perfilCriteria) {
		return perfilCriteria.listPerfil();
	}
}
