/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public interface ORMConstants extends org.orm.util.ORMBaseConstants {
	final int KEY_IMAGEN_IDUSER = 2050751154;
	
	final int KEY_PERFIL_USER = -1026678210;
	
	final int KEY_USER_IDPERFIL = 104164507;
	
	final int KEY_USER_IMAGEN = 1224569159;
	
}
