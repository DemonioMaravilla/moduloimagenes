/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PerfilCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression descripcion;
	public final CollectionExpression user;
	
	public PerfilCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		descripcion = new StringExpression("descripcion", this);
		user = new CollectionExpression("ORM_User", this);
	}
	
	public PerfilCriteria(PersistentSession session) {
		this(session.createCriteria(Perfil.class));
	}
	
	public PerfilCriteria() throws PersistentException {
		this(orm.ModuloImagenesPersistentManager.instance().getSession());
	}
	
	public UserCriteria createUserCriteria() {
		return new UserCriteria(createCriteria("ORM_User"));
	}
	
	public Perfil uniquePerfil() {
		return (Perfil) super.uniqueResult();
	}
	
	public Perfil[] listPerfil() {
		java.util.List list = super.list();
		return (Perfil[]) list.toArray(new Perfil[list.size()]);
	}
}

