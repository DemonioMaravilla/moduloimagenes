/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class User {
	public User() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_USER_IMAGEN) {
			return ORM_imagen;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_USER_IDPERFIL) {
			this.idPerfil = (orm.Perfil) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private String nick;
	
	private String pass;
	
	private String mail;
	
	private orm.Perfil idPerfil;
	
	private java.util.Set ORM_imagen = new java.util.HashSet();
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Nombre de Usuario
	 */
	public void setNick(String value) {
		this.nick = value;
	}
	
	/**
	 * Nombre de Usuario
	 */
	public String getNick() {
		return nick;
	}
	
	public void setPass(String value) {
		this.pass = value;
	}
	
	public String getPass() {
		return pass;
	}
	
	/**
	 * e-mail del usuario registrado
	 */
	public void setMail(String value) {
		this.mail = value;
	}
	
	/**
	 * e-mail del usuario registrado
	 */
	public String getMail() {
		return mail;
	}
	
	public void setIdPerfil(orm.Perfil value) {
		if (idPerfil != null) {
			idPerfil.user.remove(this);
		}
		if (value != null) {
			value.user.add(this);
		}
	}
	
	public orm.Perfil getIdPerfil() {
		return idPerfil;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_IdPerfil(orm.Perfil value) {
		this.idPerfil = value;
	}
	
	private orm.Perfil getORM_IdPerfil() {
		return idPerfil;
	}
	
	private void setORM_Imagen(java.util.Set value) {
		this.ORM_imagen = value;
	}
	
	private java.util.Set getORM_Imagen() {
		return ORM_imagen;
	}
	
	public final orm.ImagenSetCollection imagen = new orm.ImagenSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_USER_IMAGEN, orm.ORMConstants.KEY_IMAGEN_IDUSER, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
