<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>eSOS - Modulo Im�genes</title>
</head>
<body>


<div class="navbar navbar-default navbar-fixed-top" style="background: #34495e;">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand"  style="color:#ecf0f1" href="#"> Proyecto eSOS </a> 
        </div>
         <center>
            <div class="navbar-collapse collapse" id="navbar-main">
                <ul class="nav navbar-nav">
                   
                   
                </ul>
                <form class="navbar-form navbar-right" role="search" action="Login" method="POST">
                    
                   <a class="navbar-form navbar-right"  style="color:#ecf0f1">Bienvenido ${sessionScope.usuario} - Log out</a>
                </form>
            </div>
        </center>
        
    </div>
                            
</div>

<br> <br> 


	<div class="container" style="margin-top: 60px">
		<div class="row text-center">
		
			<form action="SubirImagenes" method="post" enctype="multipart/form-data">
				<label for="inputFile">Subir im�genes<br><br>
				<input type="file"  class="btn btn-default" name="fileName" multiple="multiple" id="inputFile"/> </label>
				<br> <br> 
				<input type="submit" class="btn btn-success" value="Upload" />
			</form>

		</div>
	</div>














	<div class="row text-center">

    <div class="col-lg-12">
        <h1 class="page-header">Galer�a Completa</h1>
    
            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="titulo" data-caption="prueba1" 
            data-image="img/foto1.jpg" data-target="#image-gallery">
                <img class="img-responsive" src="img/foto1.jpg">
            </a>
        </div>
            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="Titulo2" data-caption="imagen prueba 2" 
            data-image="img/foto2.jpg" data-target="#image-gallery">
                <img class="img-responsive" src="img/foto2.jpg" alt="A alt text">
            </a>
        </div>
            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="Titulo 3" data-caption="imagen prueba 3" 
            data-image="img/foto3.jpg" data-target="#image-gallery">
                <img class="img-responsive" src="img/foto3.jpg" alt="Another alt text">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="Titulo 3" data-caption="imagen prueba 3" 
            data-image="img/foto3.jpg" data-target="#image-gallery">
                <img class="img-responsive" src="img/foto3.jpg" alt="Another alt text">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="Titulo 3" data-caption="imagen prueba 3" 
            data-image="img/foto3.jpg" data-target="#image-gallery">
                <img class="img-responsive" src="img/foto3.jpg" alt="Another alt text">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="Titulo 3" data-caption="imagen prueba 3" 
            data-image="img/foto3.jpg" data-target="#image-gallery">
                <img class="img-responsive" src="img/foto3.jpg" alt="Another alt text">
            </a>
        </div>
</div>


<div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">�</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title" id="image-gallery-title"></h4>
            </div>
            <div class="modal-body">
                <img id="image-gallery-image" class="img-responsive" src="">
            </div>
            <div class="modal-footer">

                <div class="col-md-2">
                    <button type="button" class="btn btn-primary" id="show-previous-image">Anterior</button>
                </div>

                <div class="col-md-8 text-justify" id="image-gallery-caption">
                    This text will be overwritten by jQuery
                </div>

                <div class="col-md-2">
                    <button type="button" id="show-next-image" class="btn btn-default">Siguiente</button>
                </div>
            </div>
        </div>
    </div>
</div>




</body>
<script   src="http://code.jquery.com/jquery-3.1.1.js"   integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="   crossorigin="anonymous"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script type="text/javascript">

$(document).ready(function(){

    loadGallery(true, 'a.thumbnail');

    //This function disables buttons when needed
    function disableButtons(counter_max, counter_current){
        $('#show-previous-image, #show-next-image').show();
        if(counter_max == counter_current){
            $('#show-next-image').hide();
        } else if (counter_current == 1){
            $('#show-previous-image').hide();
        }
    }

    /**
     *
     * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
     * @param setClickAttr  Sets the attribute for the click handler.
     */

    function loadGallery(setIDs, setClickAttr){
        var current_image,
            selector,
            counter = 0;

        $('#show-next-image, #show-previous-image').click(function(){
            if($(this).attr('id') == 'show-previous-image'){
                current_image--;
            } else {
                current_image++;
            }

            selector = $('[data-image-id="' + current_image + '"]');
            updateGallery(selector);
        });

        function updateGallery(selector) {
            var $sel = selector;
            current_image = $sel.data('image-id');
            $('#image-gallery-caption').text($sel.data('caption'));
            $('#image-gallery-title').text($sel.data('title'));
            $('#image-gallery-image').attr('src', $sel.data('image'));
            disableButtons(counter, $sel.data('image-id'));
        }

        if(setIDs == true){
            $('[data-image-id]').each(function(){
                counter++;
                $(this).attr('data-image-id',counter);
            });
        }
        $(setClickAttr).on('click',function(){
            updateGallery($(this));
        });
    }
});</script>
</html>